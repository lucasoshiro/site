---
layout: post
categories: events
title: "Linux Install Fest"
author: spoonm
lang: en
excerpt_separator: <!--end-abstract-->
---

We helped many people "free" their computers by installing GNU/Linux!

<!--end-abstract-->

Linux Install Fest is a recurring event organized by
[CCSL](https://ccsl.ime.usp.br/en) (FLOSS Competence Center) technical director
Nelson Lago, and aims to help not only students but anyone who wishes to install
a distribution of the GNU/Linux operating system on their personal computers but
doesn't feel comfortable doing so by themselves.  This year we volunteered to
participate in the event alongside [USPCodeLab](https://codelab.ime.usp.br/) and
[IMEsec](https://imesec.ime.usp.br/), as it is perfectly aligned with our core
principles.

| **When**  | Tuesday, March 26, 2019, 1pm - 5pm |
| **Where** | IME-USP, CCSL Lab 16               |

Posters were hung around campus near bus stops to advertise the event, and it
was also mentioned on other media, such as Facebook and online radio.

After the scheduled talk by Nelson Lago at 1pm, we managed to install friendly
distributions such as Ubuntu, Linux Mint, Debian, and Manjaro on roughly 20
devices. While many of the attendees were, naturally, first-year students from
the B.Sc. Computer Science program, we also received Statistics majors and
people from outside the University of São Paulo.

We are glad to have had the opportunity to help people install a GNU/Linux
distribution on their machines. We know one of free software's success factors
is community bond. Helping people experiment with their first free operating
system is sometimes the first step towards making new friends and a great way to
raise their interest in free software. We hope we may help with many more such
events in the future. Thanks to all the attendees.

{% include add_image.html
	src="1.jpg"
	style="width: 48%; float: left; margin: 0 1%;" %}

{% include add_image.html
	src="2.jpg"
	style="width: 48%; margin: 0 1%;" %}
