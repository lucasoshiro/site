---
layout: post
categories: events
title: "KernelDevDay"
lang: en
ref: kddLandingPage2019
author: rafaeltsu
---

{% include add_multilang.html %}

KernelDevDay is a coding event organized by FLUSP (FLOSS@USP - IME-USP Extension
Group) in which participating teams are going to spend approximately 10 hours
contributing to some Linux kernel subsystem. In this first edition of the event,
we aim to contribute to some drivers from the sensors subsystem (Industrial
Input/Output - IIO) that are currently not ready to be merged into the Linux
Kernel main tree due to technical issues.

- Warmup
  - **When**: Friday, May 17, 2019 from 14 pm to 17 pm
  - **Where**: IME-USP, CCSL, CCSL meeting room (downstairs)

- KernelDevDay (main event)
  - **When**: Saturday, May 18, 2019 from 8 am to 7 pm
  - **Where**: IME-USP, CCSL, CCSL Auditorium (downstairs)

### Goals:

We have a simple and clear mission: we want to encourage the Linux kernel
community of developers in Brazil. Given FLUSP's experience with contributing to
multiple FLOSS communities, we believe we can act as a catalyzer to the process
of bringing new developers to the kernel project.

Everyone who has their code accepted into the kernel project has their names
registered in its contribution log (the famous `git log`). In other words,
contributing to the Linux kernel is a great opportunity to work with a complex,
high-impact software project. In short, your name will be immortalized in one of
the biggest and most famous FLOSS projects of the last three decades.

### The Industrial Input/Output (IIO) subsystem:

Nowadays our interaction with sensors is increasingly common through smartphones
or through Internet of Things (IoT) devices. In this context, Linux has a set of
programs that define how the interaction between the operating system and these
devices shall happen: the IIO subsystem. Among the many assignments of this
subsystem, we highlight the manipulation of accelerometers, gyroscopes, and
temperature, light, and pressure sensors.

For more information on the IIO subsystem, refer to its documentation:
[IIO documentation](https://01.org/linuxgraphics/gfx-docs/drm/driver-api/iio/index.html)

### What we will do:

Our goal is making all of the code written during the KernelDevDay be merged
into the Linux kernel main repository. With this in mind, we will focus our
efforts on an intermediate development tree called the Staging tree. This
repository aggregates drivers that aren't up to the kernel's quality standards
for several technical reasons and therefore can not be moved into the main
project tree.

During the event we will work on a pre-selected set of 3 to 5 drivers. We plan
to send relevant patches to move these drivers out of staging and enable them to
be merged into the main kernel tree.

### What is a patch?

In software development, a patch is a set of changes made to a program to fix or
improve the current version of its code. Patches may contain (but are not
limited to):

* Bug fixes
* New functionalities
* Documentation changes
* Checkstyle
* Adaptation of legacy code to the current standards

### Requirements

In order for the event to be productive, attendees must be familiar with the
following topics:
- The C programming language (macros, pointers, bitwise operators)
- Linux-CLI (Command Line Interface)
- Git (add, commit, status, log, ...)

Non-technical requirements:
- Be polite and maintain a good ethical conduct
- Do not be afraid to read hardware specifications (datasheets) in English

### Schedule:
#### Warmup (May 17h):
- **14h - 17h**: Pre-install and development environment setup
  - Clone repositories
  - Subscribe to a mailing list
  - Compile the kernel
  - Setup KW ([Kernel Workflow](https://github.com/rodrigosiqueira/kworkflow))
  - Setup git user and email
  - Install dependencies
  - Follow tutorials
    - [Kernel compilation and installation](https://flusp.ime.usp.br/others/2019/02/16/Kernel-compilation-and-installation/)
    - [IIO dummy anatomy](https://flusp.ime.usp.br/iio/2019/03/16/iio-dummy-anatomy/)
    - [Play with IIO dummy](https://flusp.ime.usp.br/iio/2019/04/20/experiment-one-iio-dummy/)
    - [Sending patches by email with git](https://flusp.ime.usp.br/git/2019/02/15/sending-patches-by-email-with-git/)

We will follow [this](https://flusp.ime.usp.br/materials/Kernel_Primeiros_Passos.pdf)
material (PT-BT) in the warmup section. If you are unable to attend the warmup,
it is highly recommended that you follow the material and make the listed
tutorials by yourself. If you have any questions in the process, do not exist
to ask us! See how to contact us [here](#contact).

#### KernelDevDay (May 18th):
- **8h30 - 9h**: Opening
  - Introduction to FLUSP and its philosophy
  - Sponsors' videos

- **9h - 9h30**: Groups, drivers and issues definition

- **9h30 - 12h**: 1st **development** phase for each group

- **11h30 - 12h**: 1st **code review** phase for the mentors

- **11h30 - 12h30**: 1st coffee-break

- **13h - 15h**: 2nd **development** phase for each group

- **15h - 16h**: 2nd **code review** phase for the mentors

- **15h - 16h**: 2nd coffee-break

- **16h - 19h**: 3rd **development** phase for each group

- **18h - 19h**: 3rd **code review** phase for the mentors and **sending
  patches**

- **19h - 20h**: Happy hour

- Free coffee and water will be available during the event. In addition, several
gifts will be distributed to participants throughout the day!

### Application form

The application is closed :( But you are welcome to join us
[here]({{ site.baseurl }}{% link about.md %}#how-to-reach-us) to know more about
our future events!
{:.warning .text-center}

### Access to the venue:

The event is going to take place in the auditorium of the University of São
Paulo's FLOSS Competence Center (CCSL). (Address: 1171, Av. Prof. Luciano
Gualberto, São Paulo). You can find more detailed information on how to reach
the venue by public transport or by car on
[CCSL's official website](http://ccsl.ime.usp.br/en/location).

To ensure that everyone can access the University of São Paulo (USP) campus
during the day of the event, **it is necessary that attendees carry their IDs or
USP affiliation card**.

### Contact:

You can also check for updates on the event and many other FLUSP activities on
our [Telegram group](https://t.me/joinchat/BDEctw1URj59ayCWY88qvA) and our
[official web page](https://flusp.ime.usp.br/)

For more information, feel free to send us an e-mail:
[flusp@googlegroups.com](mailto:flusp@googlegroups.com)

### Sponsors:

<div class="text-center">
  <a href="https://www.digitalocean.com/" class="spoonmsor_logo">
    <img src="/img/sponsors/DO.svg" width="251px">
  </a>
  <a href="https://www.analog.com/en/index.html/" class="spoonmsor_logo">
    <img src="/img/sponsors/ADI.png">
  </a>
  <a href="https://profusion.mobi/" class="spoonmsor_logo">
    <img src="/img/sponsors/ProFusion.png" width="251px">
  </a>
  <a href="https://www.collabora.com/" class="spoonmsor_logo">
    <img src="/img/sponsors/Collabora.png" width="185px">
  </a>
</div>
