---
layout: post
categories: events
title: "KernelDevDay"
lang: pt
ref: kddLandingPage2019
author: rafaeltsu
---

{% include add_multilang.html %}

O KernelDevDay é um evento de programação organizado pelo FLUSP (FLOSS@USP -
Grupo de Extensão do IME-USP) no qual as equipes participantes passam um
determinado período contíguo (aproximadamente 10h) desenvolvendo contribuições
para algum subsistema do kernel Linux. Para esta primeira edição do KernelDevDay
buscamos especialmente contribuir para alguns drivers do subsistema de sensores
(Industrial Input/Output - IIO) que ainda não estão prontos para serem
incorporados à árvore principal do Linux devido a várias razões técnicas.

- Aquecimento / preparação para o evento
  - **Quando**: Sexta-feira, 17 de Maio, das 14h às 17h
  - **Onde**:   IME-USP, CCSL, Sala de reunião do CCSL

- KernelDevDay (evento principal)
  - **Quando**: Sábado, 18 de Maio, das 8h às 19h
  - **Onde**:   IME-USP, CCSL, Auditório do CCSL

### Objetivos:

Nós temos uma missão simples e clara quanto à promoção desse evento: queremos
fomentar a comunidade de contribuidores do Linux no Brasil. Dada a experiência
do FLUSP em contribuir com diversas comunidades de software livre, acreditamos
que somos capazes de atuar como um agente catalisador neste processo de atrair
novos desenvolvedores.

Todos aqueles que têm códigos aceitos no kernel têm seus nomes registrados no
log de alterações do projeto (o famoso `git log`).  Em outras palavras,
contribuir com o kernel é uma ótima oportunidade para trabalhar com um software
complexo e de grande impacto. Em resumo, o seu nome será imortalizado em um dos
maiores e mais famosos projetos de software livre das últimas três décadas.

### O subsistema de Industrial Input/Output (IIO) do kernel Linux:

Hoje em dia é cada vez mais comum a interação com sensores, seja ela via
smartphones ou aplicações de *Internet of Things* (IoT).  Neste contexto, o
Linux possui um conjunto de programas que define como ocorre a interação do
sistema operacional com estes sensores, o subsistema IIO. Dentre as várias
atribuições deste subsistema, destacam-se a manipulação de acelerômetros,
giroscópios e de sensores de temperatura, luz e pressão.

Para mais informações sobre o subsistema de IIO, consulte a documentação:
[Documentação do IIO](https://01.org/linuxgraphics/gfx-docs/drm/driver-api/iio/index.html)

### Onde vamos trabalhar:

Nosso objetivo é fazer com que os códigos desenvolvidos no KernelDevDay cheguem
até o repositório principal do kernel Linux.  Para isso, vamos nos concentrar em
um estágio intermediário chamado Staging. Esta área agrega os códigos que não se
adequam aos padrões do kernel por diversas razões técnicas e portanto não podem
ser adicionados à árvore principal do projeto.

No KernelDevDay vamos trabalhar em um conjunto pré-selecionado de 3 a 5 drivers.
Pretendemos enviar patches relevantes para que estes drivers possam sair da
staging e entrar na árvore principal do kernel.

### Mas afinal, o que é um patch?

Um patch (do inglês "remendo") é um conjunto de modificações em um programa que
tem por objetivo corrigir ou aprimorar a versão atual do código.  Patches podem
conter (mas não são limitados a):
- Correção de bugs
- Adição de novas funcionalidades
- Escrever documentação
- Adequação às convenções de estilo de código fonte
- Adequar código antigo às especificações mais recentes

### Pré-requisitos
Para que o dia do KernelDevDay seja produtivo, é necessário que os participantes
estejam familiarizados com os seguintes tópicos:
- Linguagem de programação C (macros, ponteiros, operadores bitwise)
- Linux-CLI (Comand Line Interface)
- Git (add, commit, status, log, ...)

Requisitos não técnicos:
- Ser educado e manter uma conduta ética
- Não ter medo de ler especificações de hardware (datasheets) em inglês

### Programação:
#### Aquecimento (17 de maio):
- **14h - 17h**: Pré-instalação e configuração do ambiente de desenvolvimento
  - Clonar repositorios
  - Inscrição em lista de emails
  - Compilar o kernel
  - Instalar o KW ([Kernel Workflow](https://github.com/rodrigosiqueira/kworkflow))
  - Configurar usuário e email do git
  - Instalação de dependências
  - Tutoriais
    - [Compilação e instalação do kernel](https://flusp.ime.usp.br/others/2019/02/16/Kernel-compilation-and-installation/)
    - [Anatomia do IIO dummy](https://flusp.ime.usp.br/iio/2019/03/16/iio-dummy-anatomy/)
    - [Brincando com IIO dummy](https://flusp.ime.usp.br/iio/2019/04/20/experiment-one-iio-dummy/)
    - [Enviando patches por email com git](https://flusp.ime.usp.br/git/2019/02/15/sending-patches-by-email-with-git/)

Seguiremos [esse](https://flusp.ime.usp.br/materials/Kernel_Primeiros_Passos.pdf)
material no aquecimento. Caso você não possa comparecer, é altamente recomendado
que você siga o material e faça os tutoriais listados por conta própria. Se
tiver qualquer dúvida no processo, não exite em nos perguntar! Veja como entrar
em contato conosco [aqui](#contato).

#### KernelDevDay (18 de maio):

- **8h30 - 9h**: Abertura
  - Apresentação do FLUSP e da filosofia do grupo
  - Vídeos dos patrocinadores

- **9h - 9h30**: Definição de grupos, drivers e issues

- **9h30 - 12h**: 1ª fase de **trabalho** dos grupos

- **11h30 - 12h**: 1ª fase de **revisão** pelos mentores

- **11h30 - 12h30**: Primeiro Coffee-Break

- **13h - 15h**: 2ª fase de **trabalho** dos grupos

- **15h - 16h**: 2ª fase de **revisão** pelos mentores

- **15h - 16h**: Segundo Coffee-Break

- **16h - 19h**: 3ª fase de **trabalho** dos grupos

- **18h - 19h**: 3ª fase de **revisão** pelos mentores e envio dos patches

- **19h - 20h**: Happy hour

- Café e água estarão disponíveis gratuitamente durante o evento. Além disso,
serão distribuidos diversos brindes aos participantes, ao longo do dia!

### Formulário de Inscrição

O formulário já foi encerrado :( Mas você pode se juntar a nós
[aqui]({{ site.baseurl }}{% link about.md %}#how-to-reach-us) para saber mais de
nossos próximos eventos!
{:.warning .text-center}

### Acesso ao local do evento:

O evento será realizado no auditório do Centro de Competência em Software Livre
da Universidade de São Paulo, Campus Butantã (endereço: Av. Prof. Luciano
Gualberto, 1171, São Paulo). A página oficial do
[CCSL](http://ccsl.ime.usp.br/pt-br/localizacao) tem um guia com mais detalhes
sobre como chegar via transporte público ou particular.

Para garantir que todos tenham acesso ao campus durante o dia do evento, **é
necessário que os participantes estejam portando RG ou carteirinha com Número
USP**.

### Contato:

Você poderá obter atualizações sobre esta e outras diversas atividades do FLUSP
através de nosso grupo no
[Telegram](https://t.me/joinchat/BDEctw1URj59ayCWY88qvA) e de nossa [página
oficial](https://flusp.ime.usp.br/).

E-mail para contato: [flusp@googlegroups.com](mailto:flusp@googlegroups.com)

### Patrocinadores:

<div class="text-center">
  <a href="https://www.digitalocean.com/" class="spoonmsor_logo">
    <img src="/img/sponsors/DO.svg" width="251px">
  </a>
  <a href="https://www.analog.com/en/index.html/" class="spoonmsor_logo">
    <img src="/img/sponsors/ADI.png">
  </a>
  <a href="https://profusion.mobi/" class="spoonmsor_logo">
    <img src="/img/sponsors/ProFusion.png" width="251px">
  </a>
  <a href="https://www.collabora.com/" class="spoonmsor_logo">
    <img src="/img/sponsors/Collabora.png" width="185px">
  </a>
</div>
