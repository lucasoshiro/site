---
layout: post
categories: events
title: "Guest lecture: Continuous Integration with Linux Kernel"
author: marcelosc
lang: en
excerpt_separator: <!--end-abstract-->
---

We are celebrating our third talk from free software developer by announcing a
lecture by Charles Oliveira from Linaro.

<!--end-abstract-->

This time we will be privileged to have [Linaro](https://www.linaro.org/)'s
software engineer Charles Oliveira, talking about
"**Kernel CI and Free Software Career**".

|**When**  | Tuesday, April 26, 2019, at 2pm |
|**Where** | IME-USP, CCSL autitorium        |

#### About the speaker
Charles Oliveira holds a degree in Software Engineering from the University of
Brasília. He started his open source career at college as a member of the new
Brazilian Public Software Portal. Afterward, he worked with code static
analysis for security at NIST, USA, for four years. In 2018 he was hired by
Linaro Systems Laboratory group, where he works assisting the development of a
continuous integration reporting platform. 
 
#### About the lecture
The concept of Continuous Integration (CI), has been disseminated in several
software projects through Travis, CircleCI, Jenkins, among others. Integration
usually happens at the application level, where packages and programs are
installed to ensure that the final application is being built correctly. When
integration also involves the operating system and specialized software for
embedded systems, complexity changes focus. This talk covers how low-level
continuous integration takes place on the Linaro infrastructure and why it is
essential.

*Note: The lecture will be presented in portuguese.*

<!--You can watch it [here](https://www.youtube.com/watch?v=k5AfwiKVXJc).-->
