---
layout: post
categories: events
title: "Guest lecture: Reproducible Software Builds"
author: andersonreis
lang: en
excerpt_separator: <!--end-abstract-->
---

We are celebrating our fourth lecture on free software development. In
partnership with [IMEsec](https://imesec.ime.usp.br/), we are pleased to
announce the lecture given by [Seth
Schoen](https://www.eff.org/about/staff/seth-schoen) from the [Electronic
Frontier Foundation (EFF)](https://eff.org/).

This time we will be privileged to have EFF’s senior staff technologist Seth
Schoen talking about “Reproducible Software Builds, Supply-Chain Security, and
Risks for Source-Binary Correspondence”.

<!--end-abstract-->

|**When**  | Tuesday, June 11, 2019, at 2pm |
|**Where** | IME-USP, CCSL, CCSL Auditorium (downstairs) |

#### About the speaker

Schoen works for the Electronic Frontier Foundation as a senior staff
technologist. He was a co-author of "Lest We Remember: Cold Boot Attacks on
Encryption Keys", helped create the LNX-BBC live CD and the Let's Encrypt
certificate authority, and has presented at conferences and meetings in more
than ten countries. He has testified before several courts and regulatory
agencies in the United States and trained government officials and journalists
in Latin America about privacy and information security topics.

#### About the lecture

Seth is reprising and updating a topic about which he presented at several
conferences a few years ago. As the issue of supply-chain security for IT
products is constantly back in the news, Seth will consider the problem of how
we know that the binary code we create or receive from others matches the
source code from which it was supposedly created. He will demonstrate an
attack where a compromised workstation produces malicious binaries from
non-malicious source code. This also touches on an enormous range of
supply-chain security concerns, including those related to the integrity of
software updates, and on valuable work on reproducible compilation being done
by the Debian Project, Tor Project, and Mozilla, among others.

*Note: This talk will be presented in Portuguese; audience questions are
welcomed in English, Portuguese, or Spanish.*
