---
title:  "KWorkflow"
image:  "kw"
ref: "kworkflow"
categories: "projects.md"
id: 2
---

# Description

Set of scripts to reduce overhead with Linux

# Recommendations

* Bash
* Virtual Machines
* QEMU and LibVirt
* Regex

# Contributors

Siqueira, Giuliano

# Mentors

Siqueira, Giuliano

# URLs

* [Kworkflow Repository](https://github.com/rodrigosiqueira/kworkflow)
